//
// Created by vatbon on 12.11.2018.
//
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <utility>
#include <algorithm>
#include "version.h"
#include "library.h"


#define PAIR std::make_pair

struct{
	bool operator()(Library a, Library b) const{
		if (a.get_name() > b.get_name()){
			return true;
		}
		else{
			if(a.get_name() == b.get_name()){
				if(a.get_version() > b.get_version())
					return true;
			}
			else
				return false;
			return false;
		}
	}
} customMore;

struct{
	bool operator()(Library a, Library b) const{
		if (a.get_name() < b.get_name()){
			return true;
		}
		else{
			if(a.get_name() == b.get_name()){
				if(a.get_version() < b.get_version())
					return true;
			}
			else
				return false;
			return false;
		}
	}
} customLess;

int ReadLibs(std::vector<Library>& Libs, std::istream& in_stream){
	std::string inp_val;
	std::string tmp_name, tmp_vers, tmp_rel;
	Library tmp_lib;
	size_t ppos = 0;
	tmp_name.clear(); tmp_vers.clear(); tmp_rel.clear(); inp_val.clear();
	//'-' or '+'  or else
	while (std::getline(in_stream, inp_val)){
		if(inp_val[0] == '+'){
			inp_val.erase(0, 2);
			ppos = inp_val.find_first_of(' ', 0);
			if(ppos == std::string::npos){
				tmp_lib.set_name(inp_val);
				Libs[Libs.size()-1].add_dependence(tmp_lib, "$$");
			}else
			{
				tmp_name = inp_val.substr(0, ppos);
				inp_val.erase(0, ppos + 1);
				ppos = inp_val.find_first_of(' ', 0);
				tmp_rel = inp_val.substr(0, ppos);
				inp_val.erase(0, ppos + 1);
				tmp_vers = inp_val;
				tmp_lib.set_name(tmp_name);
				tmp_lib.set_version(tmp_vers);
				Libs[Libs.size()-1].add_dependence(tmp_lib, tmp_rel);
			}
		}else
			if(inp_val[0] == '-'){
				inp_val.erase(0, 2);
				ppos = inp_val.find_first_of(' ', 0);
				if(ppos == std::string::npos){
					tmp_lib.set_name(inp_val);
					Libs[Libs.size()-1].add_conflict(tmp_lib, "$$");
				}else
				{
					tmp_name = inp_val.substr(0, ppos);
					inp_val.erase(0, ppos + 1);
					ppos = inp_val.find_first_of(' ', 0);
					tmp_rel = inp_val.substr(0, ppos);
					inp_val.erase(0, ppos + 1);
					tmp_vers = inp_val;
					tmp_lib.set_name(tmp_name);
					tmp_lib.set_version(tmp_vers);
					Libs[Libs.size()-1].add_conflict(tmp_lib, tmp_rel);
				}
			}else
			{
				ppos = inp_val.find_first_of(' ', 0);
				if(ppos != std::string::npos){
					tmp_name = inp_val.substr(0, ppos);
					inp_val.erase(0, ppos + 1);
					tmp_vers = inp_val;
					tmp_lib.set_name(tmp_name);
					tmp_lib.set_version(tmp_vers);
					Libs.push_back(tmp_lib);
				}
			}
		tmp_name.clear(); tmp_vers.clear(); tmp_rel.clear();
	}
	return 0;
}


int ReadTargets(std::vector<Library>& Libs, std::istream& in_stream){
	std::string inp_val;
	std::string tmp_name, tmp_vers, tmp_rel;
	Library tmp_lib;
	size_t ppos = 0;
	tmp_name.clear(); tmp_vers.clear(); tmp_rel.clear(); inp_val.clear();
	//'-' or '+'  or else
	while (std::getline(in_stream, inp_val)){
		ppos = inp_val.find_first_of(' ', 0);
		if(ppos != std::string::npos){
			tmp_name = inp_val.substr(0, ppos);
			inp_val.erase(0, ppos + 1);
			ppos = inp_val.find_first_of(' ', 0);
			tmp_rel = inp_val.substr(0, ppos);
			inp_val.erase(0, ppos + 1);
			tmp_vers = inp_val;
			if ((tmp_rel == ">") || (tmp_rel == "<"))
				tmp_rel.append(tmp_rel);
			tmp_lib.set_name((tmp_rel + tmp_name));
			tmp_lib.set_version(tmp_vers);
		}
		else{
			tmp_name = inp_val;
			tmp_lib.set_name(("$$" + tmp_name));
			tmp_lib.set_version("0");
		}
		Libs.push_back(tmp_lib);
		tmp_name.clear(); tmp_vers.clear(); tmp_rel.clear();
	}
	return 0;
}


std::vector<Library> BuildTargets(std::vector<Library>& Libs, std::istream& InputTargets, int* exit_code){
	std::vector<Library> Targets;
	std::vector<Library*> Solution;
	std::sort(Libs.begin(), Libs.end(), customMore);
	ReadTargets(Targets, InputTargets);
	std::map<std::pair<Library*, Library*>, char> relat;
	for (auto& lib1 : Libs){
		for(auto& lib2 : Libs){
			relat[PAIR(&lib1, &lib2)] = relation(lib1, lib2);
		}
	}
	for(auto& targ : Targets)
		for(auto& lib : Libs){
			if ( lib.get_name() == targ.get_name().substr(2, targ.get_name().size() - 2) )
				if (!lib.match(targ))
					relat[PAIR(&lib, &lib)] = 0;//doesn't match the target
				else
					relat[PAIR(&lib, &lib)] = 2;//matches the target
		}
	bool tmp_trig = false,
		exit_success = false,
	     exit_fail = false;
	int i, j, z;
	Library *sol, *it_lib, *itt_lib;
	Library tmp_lib;
	std::vector<Library> checkconf;
	
	if (!(exit_success) && !(exit_fail)){
		tmp_trig = false;
		for (auto& targ : Libs){ 
			if (relat[PAIR(&targ,&targ)] == 2){
				Solution.push_back(&targ);
				relat[PAIR(&targ,&targ)] = 0;
				tmp_trig = true;
				it_lib = &targ;
			}// found first target

				for (auto& tmp : Solution)
					checkconf.push_back(*tmp);
				if (has_conf(checkconf)){
					Solution.pop_back();
				}
			z = 0;
			while (z < Solution.size()){
				sol = Solution[z];
				i = sol->get_depes().size();// number of depes
				j = i;
				checkconf.clear();
				for (auto& lib : Libs){// add all depes
					if (i != 0){
						if (relat[PAIR(sol, &lib)] == 2){
							Solution.push_back(&lib);
							for (auto& tmp : Solution)
								checkconf.push_back(*tmp);
							if (has_conf(checkconf)){
								Solution.pop_back();
							}
							else{
								i--;
							}
							checkconf.clear();
						}
					}	
				}
				z++;

				if (i != 0){ // if all depes not covered
					while (j != i){
						Solution.pop_back();
						i++;
					}	
					Solution.pop_back();
				}
			}
		}
	}
	if ((Solution.size() == 0))
		exit_fail = true;
	else
		exit_success = true;	
	if (exit_success){
		*exit_code = 0;
	}
	if (exit_fail){
		*exit_code = 1;
	}
	for (auto& tmp : Solution)
		checkconf.push_back(*tmp);
	return checkconf;
}


int main(int argc, char** argv){
	std::ifstream InputFileLibs ("libs.txt", std::ios::in);
	std::ifstream InputFileTargets ("targets.txt", std::ios::in);
	if( !(InputFileLibs.is_open()) )
		return 1;
	if( !(InputFileTargets.is_open()) )
		return 1;
	std::vector<Library> Libs, dep, con;
	ReadLibs(Libs, InputFileLibs);
	unsigned int i,j;
	std::vector<Library> Solution;
	int exit_code = 0;
	Solution = BuildTargets(Libs, InputFileTargets, &exit_code);
	if (exit_code == 0){
		std::sort(Solution.begin(), Solution.end(), customLess);
		for (auto& lib : Solution)
			std::cout << lib.get_name() << " " << lib.get_version() << std::endl;
		return 0;
	}
	else if (exit_code == 1){
		std::cerr << "Unable to resolve targets.\n";
		return 1;
	}
	return 0;
}
