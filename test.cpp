#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "version.h"
#include "library.h"

TEST_CASE("Library->Version","compare"){
	Library lib1, lib2;
	lib1.set_version("1");
	lib2.set_version("2");
	REQUIRE( !(lib1.get_version() > lib2.get_version()) );
	REQUIRE( !(lib1.get_version() >= lib2.get_version()) );
	REQUIRE( (lib1.get_version() != lib2.get_version()) );
	REQUIRE( (lib1.get_version() < lib2.get_version()) );
	REQUIRE( (lib1.get_version() <= lib2.get_version()) );
	REQUIRE( !(lib1.get_version() == lib2.get_version()) );
	lib2.set_version("1");
	REQUIRE( !(lib1.get_version() > lib2.get_version()) );
        REQUIRE( (lib1.get_version() >= lib2.get_version()) );
        REQUIRE( !(lib1.get_version() != lib2.get_version()) );
        REQUIRE( !(lib1.get_version() < lib2.get_version()) );
        REQUIRE( (lib1.get_version() <= lib2.get_version()) );
        REQUIRE( (lib1.get_version() == lib2.get_version()) );
	lib1.set_version("1.1");
	lib2.set_version("1.2");
        REQUIRE( !(lib1.get_version() > lib2.get_version()) );
        REQUIRE( !(lib1.get_version() >= lib2.get_version()) );
        REQUIRE( (lib1.get_version() != lib2.get_version()) );
        REQUIRE( (lib1.get_version() < lib2.get_version()) );
        REQUIRE( (lib1.get_version() <= lib2.get_version()) );
        REQUIRE( !(lib1.get_version() == lib2.get_version()) );
	lib1.set_version("0.1");
	lib2.set_version("1.2");
        REQUIRE( !(lib1.get_version() > lib2.get_version()) );
        REQUIRE( !(lib1.get_version() >= lib2.get_version()) );
        REQUIRE( (lib1.get_version() != lib2.get_version()) );
        REQUIRE( (lib1.get_version() < lib2.get_version()) );
        REQUIRE( (lib1.get_version() <= lib2.get_version()) );
        REQUIRE( !(lib1.get_version() == lib2.get_version()) );
	lib1.set_version("0.6.2.8");
	lib2.set_version("0.6.2.8");
	REQUIRE( !(lib1.get_version() > lib2.get_version()) );
        REQUIRE( (lib1.get_version() >= lib2.get_version()) );
        REQUIRE( !(lib1.get_version() != lib2.get_version()) );
        REQUIRE( !(lib1.get_version() < lib2.get_version()) );
        REQUIRE( (lib1.get_version() <= lib2.get_version()) );
        REQUIRE( (lib1.get_version() == lib2.get_version()) );
	lib1.set_version("2.0");
	lib2.set_version("1.1");
	REQUIRE( (lib1.get_version() > lib2.get_version()) );
        REQUIRE( (lib1.get_version() >= lib2.get_version()) );
        REQUIRE( (lib1.get_version() != lib2.get_version()) );
        REQUIRE( !(lib1.get_version() < lib2.get_version()) );
        REQUIRE( !(lib1.get_version() <= lib2.get_version()) );
        REQUIRE( !(lib1.get_version() == lib2.get_version()) );
	lib1.set_version("1.0");
	lib2.set_version("1.1");
	REQUIRE( !(lib1.get_version() > lib2.get_version()) );
        REQUIRE( !(lib1.get_version() >= lib2.get_version()) );
        REQUIRE( (lib1.get_version() != lib2.get_version()) );
        REQUIRE( (lib1.get_version() < lib2.get_version()) );
        REQUIRE( (lib1.get_version() <= lib2.get_version()) );
        REQUIRE( !(lib1.get_version() == lib2.get_version()) );

}

TEST_CASE("Library", "is_match"){
	Library lib1, lib2;
	lib1.set_name("Alpha");
	lib1.set_version("1.0");
	lib2.set_name("==Alpha");
	lib2.set_version("1.0");
	REQUIRE(lib1.match(lib2));
	lib2.set_name(">=Alpha");
	REQUIRE(lib1.match(lib2));
	lib2.set_name(">Alpha");
	REQUIRE(!(lib1.match(lib2)));
	lib2.set_name("$$Alpha");
	REQUIRE(lib1.match(lib2));
	lib2.set_name("==NotAlpha");
	REQUIRE(!(lib1.match(lib2)));
}
