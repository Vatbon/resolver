//
// Created by vatbon on 12.11.2018.
//

#include <string>
#include <cstdlib>
#include "version.h"

void version::set_vers(const std::string& NewVersion)
{
    number.clear();
    std::string _NewVersion;
    size_t ppos;
    _NewVersion.assign(NewVersion);
    while(_NewVersion.size()){
        ppos = _NewVersion.find_first_of('.', 0);
        if(ppos == std::string::npos){
            number.push_back(std::stoul(_NewVersion, nullptr , 10));
	    _NewVersion.clear();
        }
        else
        {
            number.push_back(std::stoul(_NewVersion.substr(0, ppos), nullptr, 10));
	    _NewVersion.erase(0, ppos + 1);
        }
    }
}

bool version::operator>(const version& RightV) const
{	
	int i = 0;
	bool res = false;
    bool less = false;
    bool more = false;
    unsigned int End_of_iteration = number.size() < RightV.number.size() ? number.size() : RightV.number.size();
    while ((i < End_of_iteration) && !(less) && !(more) ){
        if(number[i] < RightV.number[i])
            less = true;
        if(number[i] > RightV.number[i])
            more = true;
	i++;
    }
    if (less)
	    res = false;
    if (more)
	    res = true;
    if (!(more) && !(less) && (number.size() < RightV.number.size()))
	    res = false;
    return res;
}

bool version::operator==(const version& RightV) const
{
    bool result = true;
    unsigned int i;
    unsigned int End_of_iteration = number.size() < RightV.number.size() ? number.size() : RightV.number.size();
    if (number.size() != RightV.number.size())
        result = false;
    else{
        for(i = 0; i < End_of_iteration; i++)
            if(number[i] != RightV.number[i])
                result = false;
    }
    return result;
}

bool version::operator<(const version& RightV) const
{
    return RightV > *this;
}

bool version::operator>=(const version& RightV) const
{
    return (*this > RightV)||(*this == RightV);
}

bool version::operator<= (const version& RightV) const
{
    return RightV >= *this;
}

bool version::operator!= (const version& RightV) const
{
    return !(*this == RightV);
}

version& version::operator=(const version& NewVersion)
{
    number.clear();
    for(unsigned int i = 0 ; i < NewVersion.number.size(); i++)
        number.push_back(NewVersion.number[i]);
    return *this;
}

std::ostream& operator<<(std::ostream& out, const version& CurrVers)
{
	std::string str;
	str.clear();
	str = std::to_string(CurrVers.number[0]);
	unsigned int i;
	for(i = 1; i < CurrVers.number.size(); i++){
		str.append(".");
		str.append(std::to_string(CurrVers.number[i]));
	}
	out << str;
	str.clear();
	return out;
}

