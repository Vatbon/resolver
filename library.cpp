//
// Created by vatbon on 12.11.2018.
//

#include <string>
#include <vector>
#include "version.h"
#include "library.h"

void Library::set_version(const std::string& NewVersion)
{
	vers.set_vers(NewVersion);
}

version Library::get_version() const
{
	return vers;
}

void Library::set_version(const version& NewVersion)
{
	vers = NewVersion;
}

void Library::set_version(const char*& NewVersion)
{
	std::string NewVersion_str;
	NewVersion_str.assign(NewVersion);
	vers.set_vers(NewVersion_str);
	NewVersion_str.clear();
}

std::string Library::get_name() const
{
	return name;
}

void Library::set_name(const std::string& NewName)
{
	name.assign(NewName.data());
}

void Library::add_conflict(const Library& NewConflict, std::string Relation)
{
	Library temp_lib;
	if ((Relation == ">") || (Relation == "<"))
		Relation.append(Relation);
	temp_lib.set_name((Relation + NewConflict.get_name()));
	temp_lib.set_version(NewConflict.get_version());
	conflicts.push_back(temp_lib);
}

void Library::add_dependence(const Library& NewDependence, std::string Relation)
{
	Library temp_lib;
	if ((Relation == ">") || (Relation == "<"))
		Relation.append(Relation);
	temp_lib.set_name((Relation + NewDependence.get_name()));
	temp_lib.set_version(NewDependence.get_version());
	dependecies.push_back(temp_lib);
}

std::vector<Library> Library::get_depes() const
{
	return dependecies;
}

std::vector<Library> Library::get_confs() const
{
	return conflicts;
}

char relation(const Library& lib1, const Library& lib2){ // 0 - conflict 1 - no conf  2 - depends
	char res = 1;
	char conf = 0;
	if (&lib1 == &lib2)
		return 1;
	else
		if(lib1.get_name() == lib2.get_name())
			return 0;
	std::vector<Library> confs = lib1.get_confs();
	std::vector<Library> deps = lib1.get_depes();
	int i;
	Library rel_lib;
	for(i = 0; i < confs.size(); i++){
		rel_lib = confs[i];
		if(lib2.match(rel_lib))
			conf = 1;
	}
	for(i = 0; i < deps.size(); i++){
		rel_lib = deps[i];
		if(lib2.match(rel_lib)){
			res = 2;
		}
		else{
			if (lib2.get_name() == rel_lib.get_name().substr(2, rel_lib.get_name().size()))
				conf = 1;
		}

	}
	if (conf)
		res = 0;
	return res;
}

bool Library::match(const Library& a) const
{
	bool result = false;
	enum relat {neq, eq, mr, mreq, ls, lseq, ex};
	unsigned char rel = neq;
	std::string relation;
	relation = a.get_name().substr(0, 2);
	if (relation == "!=")
		rel = neq;
	else
		if (relation == "==")
			rel = eq;
		else
			if (relation == ">>")
				rel = mr;
			else
				if (relation == ">=")
					rel = mreq;
				else
					if (relation == "<<")
						rel = ls;
					else
						if (relation == "<=")
							rel = lseq;
						else
							if (relation == "$$")
								rel = ex;
	//Reusing relation for renaming a;
	relation = a.get_name();
	relation.erase(0, 2);
	if (name != relation)
		return false;
	switch(rel){
		case neq:
			if (vers != a.get_version())
				result = true;
			break;
		case eq:
			if (vers == a.get_version())
				result = true;
			break;
		case mr:
			if (vers > a.get_version())
				result = true;
			break;
		case mreq:
			if (vers >= a.get_version())
				result = true;
			break;
		case ls:
			if (vers < a.get_version())
				result = true;
			break;
		case lseq:
			if (vers <= a.get_version())
				result = true;
			break;
		case ex:
			if (name == relation)
				result = true;
			break;
	}
	return result;
}

Library& Library::operator= (const Library& nlib){
	if(this != &nlib){
		vers = nlib.get_version();
		name = nlib.get_name();
		dependecies = nlib.get_depes();
		conflicts = nlib.get_confs();
	}
	return *this;
}

bool has_conf(const std::vector<Library>& Libs){
	bool res = false;
	
	for (auto& lib1 : Libs){
		for(auto& lib2 : Libs){
			if (relation(lib1, lib2) == 0)
				res = true;
		}
	}
	return res;
}

