//
// Created by vatbon on 12.11.2018.
//

#ifndef PROJECT_VERSION_H
#define PROJECT_VERSION_H


#include <vector>
#include <string>
#include <stdlib.h>
#include <iostream>

class version{
protected:
    std::vector<unsigned int> number;
public:
    void set_vers(const std::string&);
    version& operator=(const version&);
    bool operator>(const version&) const;
    bool operator<(const version&) const;
    bool operator==(const version&) const;
    bool operator>=(const version&) const;
    bool operator<=(const version&) const;
    bool operator!=(const version&) const;
    friend std::ostream& operator<<(std::ostream&,const version&);
};



#endif //PROJECT_VERSION_H
