//
// Created by vatbon on 12.11.2018.
//

#ifndef PROJECT_LIBRARY_H
#define PROJECT_LIBRARY_H


#include <string>
#include <vector>
#include "version.h"

class Library{
protected:
    version vers;
    std::string name;
    std::vector<Library> dependecies;
    std::vector<Library> conflicts;
    //Relations: != == >> >= << <= $$ 
    //$$ - existance
public:
    version get_version() const;
    void set_version(const std::string&);
    void set_version(const version&);
    void set_version(const char*&);
    std::string get_name() const;
    void set_name(const std::string&);
    void add_conflict(const Library&, std::string);
    void add_dependence(const Library&, std::string);
    std::vector<Library> get_depes() const;
    std::vector<Library> get_confs() const;
    bool match(const Library&) const;
    Library& operator= (const Library&);    
};

char relation(const Library& , const Library&);
bool has_conf(const std::vector<Library>&);
#endif //PROJECT_LIBRARY_H
